# This python file tracks the incremental DB schema changes.
# Workflow:
#   Every time when your schema changes supply that as a migration along with it's rollback,
#   You are responsible for supplying proper DDL and rollback statements
#   CURRENT_MIGRATION is what your intended migration point, which will be picked my `migrate.py`
#   The script `migrate.py` first goes and checks the latest applied migration,
#   If the migration_id is less that currently available migration_dict in sourcecode, It starts applying the remaining migrations
#   If the migration_id exceeds the currently migration in source code, it starts rolling back until it reaches the required migration_id

CURRENT_MIGRATION = 1

migrations = {
    1: {
        'sql': """CREATE TABLE "user" (
                  "id" SERIAL PRIMARY KEY,
                  "firstname" TEXT NOT NULL,
                  "lastname" TEXT NOT NULL,
                  "email" TEXT NOT NULL,
                  "password" TEXT NOT NULL,
                  "auth_token" TEXT NOT NULL,
                  "auth_expiry" TIMESTAMP,
                  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                )
              """,
        'rollback': """DROP TABLE "user" """
    }
}
