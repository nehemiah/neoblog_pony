from bottle import route, run


@route('/')
def index():
    return {'hello': 'neo'}


run(host='localhost', port=8000)
