from pony.orm import Required, Optional
from werkzeug.security import check_password_hash
from datetime import datetime
from settings import db


class User(db.Entity):
    firstname = Required(str)
    lastname = Required(str)
    email = Required(str, unique=True)
    password = Required(str)
    auth_token = Optional(str)
    auth_expiry = Optional(datetime)
    created_at = Required(datetime, sql_default='CURRENT_TIMESTAMP')

    def check_password(self, plain_password):
        return check_password_hash(self.password, plain_password)

    @property
    def name(self):
        return self.firstname

    @property
    def fullname(self):
        return '{} {}'.format(self.firstname, self.lastname)

    def __str__(self):
        return self.email

    @property
    def dict(self):
        return {"email": self.email, "name": self.name}
