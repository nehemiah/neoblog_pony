import psycopg2

from migrations import CURRENT_MIGRATION, migrations
from settings import DB_HOST, DB_PASSWORD, DB_USER, DB_NAME


# Logic (Self explanatory)
# If nothing exists creation migrations table.
# run_sql -> create table if not exists (id, rollback)
# run_sql -> insert into migrations values (0, 'dummy')

# Read value CURRENT_MIGRATION from migrations.py
# max_id = Select max(id) from migrations table

# While `max(id)` < CURRENT_MIGRATION:
# run_sql -> migrations[max_id+1]['sql']
# run_sql -> "insert into migrations values (max_id+1,migrations[max_id+1]['rollback']"
# max_id+=1

# While max_id > CURRENT_MIGRATION:
# run_sql -> migrations[max_id+1]['roll_back']
# run_sql -> delete from migrations where id=max_id
# max_id-=1

def table_exists(cur, table):
    ret = False
    try:
        select = """select exists(select relname from pg_class where relname=(%s))"""
        cur.execute(select, (table,))
        ret = cur.fetchone()[0]
    except psycopg2.Error as e:
        return ret
    return ret


conn = psycopg2.connect(database=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST)

cur = conn.cursor()
if not table_exists(cur, 'migrations'):
    cur.execute("""create table "migrations" (
                      "id" smallint primary key,
                      "sql" TEXT not null,
                      "rollback" TEXT not null,
                      "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)""")
    cur.execute("""insert into migrations(id, "sql", "rollback") values(0, 'dummy','dummy')""")

cur.execute("""select max(id) from migrations""")
max_id = cur.fetchone()[0]

while max_id < CURRENT_MIGRATION:
    mig = migrations[max_id + 1]
    cur.execute(mig['sql'])
    insert = """insert into migrations(id, "sql", "rollback") values((%s), (%s), (%s))"""
    cur.execute(insert, (max_id + 1, mig['sql'], mig['rollback']))
    max_id += 1

while max_id > CURRENT_MIGRATION:
    mig = migrations[max_id]
    cur.execute(mig['rollback'])
    delete = """delete from migrations where id=(%s)"""
    cur.execute(delete, (max_id,))
    max_id -= 1

conn.commit()
conn.close()
