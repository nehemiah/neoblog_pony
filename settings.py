from pony.orm import Database, sql_debug

DB_HOST = 'localhost'
DB_NAME = 'neoblog'
DB_USER = 'neoblog'
DB_PASSWORD = 'neoblog'

db = Database()
db.bind('postgres', user=DB_USER, password=DB_PASSWORD, host=DB_HOST, database=DB_NAME)
sql_debug(True)
